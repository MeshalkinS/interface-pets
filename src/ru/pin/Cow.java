package ru.pin;

/**
 * Created by admin on 08.11.2015.
 */
 public class Cow implements Pets {
    @Override
    public String nameAnimal() {
        return ("Корова");
    }

    @Override
    public String talkAnimal() {
        return ("Муууу");
    }

    @Override
    public String inputStr() {
        return (nameAnimal() + " говорит " + talkAnimal());
    }
}
