package ru.pin;

/**
 * Created by admin on 08.11.2015.
 */
public class Chicken implements Pets{
    @Override
    public String nameAnimal() {
        return ("Курица");
    }

    @Override
    public String talkAnimal() {
        return ("Ко - ко - ко");
    }

    @Override
    public String inputStr() {
        return (nameAnimal() + " говорит " + talkAnimal());
    }
}
