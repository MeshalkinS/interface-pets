package ru.pin;

/**
 * Created by admin on 08.11.2015.
 */
public class Dog implements Pets {
    @Override
    public String nameAnimal() {
        return ("Собака");
    }

    @Override
    public String talkAnimal() {
        return ("Гав - Гав");
    }

    @Override
    public String inputStr() {
        return (nameAnimal() + " говорит " + talkAnimal());
    }
}
