package ru.pin;

/**
 * Created by admin on 08.11.2015.
 */
public class Cat implements Pets {
    @Override
    public String nameAnimal() {
        return ("Кошка");
    }

    @Override
    public String talkAnimal() {
        return ("Мур - мур");
    }

    @Override
    public String inputStr() {
        return (nameAnimal() + " говорит " + talkAnimal());
    }
}
